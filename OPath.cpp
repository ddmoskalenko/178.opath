#include <iostream>
#include "limits.h"
#include <list>
#include <vector>
#include <algorithm>


using namespace std;

string ltrim(const string&);
string rtrim(const string&);
vector<string> split(const string&);

int _n = 0;
int _m = 0;
std::vector<std::vector<int>> mapa;
int res = INT_MAX;

struct step
{
    int cx;
    int cy;
    int val;
};

std::vector<step> makeStep(int x, int y, std::vector<std::vector<int>> arr, int preval)
{
    std::vector<step> steps;
    step temp;

    int prev = arr[y][x];

    if (_m != 1)
    {
        if (x > 0)
        {
            int val = abs(arr[y][x - 1] - prev);
            if (mapa[0][0] > val)
            {
                temp.cx = x - 1;
                temp.cy = y;
                temp.val = val;
                steps.push_back(temp);
            }
        }
    }

    if (_n != 1)
    {
        if (y > 0)
        {
            int val = abs(arr[y - 1][x] - prev);
            if (mapa[0][0] > val)
            {
                temp.cx = x;
                temp.cy = y - 1;
                temp.val = val;
                steps.push_back(temp);
            }
        }
    }

    if (_m != 1)
    {
        if (x < _m - 1)
        {
            int val = abs(arr[y][x + 1] - prev);
            if (mapa[0][0] > val)
            {
                temp.cx = x + 1;
                temp.cy = y;
                temp.val = val;
                steps.push_back(temp);
            }

        }
    }

    if (_n != 1)
    {
        if (y < _n - 1)
        {
            int val = abs(arr[y + 1][x] - prev);
            if (mapa[0][0] > val)
            {
                temp.cx = x;
                temp.cy = y + 1;
                temp.val = val;
                steps.push_back(temp);
            }
        }
    }
    std::sort(steps.begin(), steps.end(), [](const step& a, const step& b) -> bool
        {
            return a.val < b.val;
        });
    return steps;
}

std::vector<step> goPossibleFirst(int x, int y, std::vector<std::vector<int>> arr)
{
    std::vector<step> steps;
    step temp;
    int prev = arr[_n - 1][_m - 1];
   
    //y
    if (_n != 1)
    {
        auto val = abs(arr[y - 1][x] - prev);
        temp.cx = x;
        temp.cy = y - 1;
        temp.val = val;
        steps.push_back(temp);
    }

    //x
    if (_m != 1)
    {
        auto val = abs(arr[y][x - 1] - prev);
        temp.cx = x - 1;
        temp.cy = y;
        temp.val = val;
        steps.push_back(temp);
    }

    std::sort(steps.begin(), steps.end(), [](const step& a, const step& b) -> bool
        {
            return a.val < b.val;
        });

    return steps;
}

void BackTrace(int x, int y, std::vector<std::vector<int>> cost, int prevX, int prevY)
{
    if (x == 0 && y == 0)   return;

    int preval = mapa[y][x];

    std::vector<step> cells;
    if (y == _n - 1 && x == _m - 1) cells = goPossibleFirst(x, y, cost);
    else cells = makeStep(x, y, cost, preval);

    for (long unsigned int i = 0; i < cells.size(); i++)
    {
        if (cells[i].cx == prevX && cells[i].cy == prevY) continue;

        if (cells[i].cx == _m - 1 && cells[i].cy == _n - 1) continue;
        int val = max(cells[i].val, preval);
        if ((mapa[0][0] > val && mapa[cells[i].cy][cells[i].cx] > val))
        {
            mapa[cells[i].cy][cells[i].cx] = val;
            BackTrace(cells[i].cx, cells[i].cy, cost, x, y);
        }
    }
    return;
}

int getMinEffort(vector<vector<int>> C)
{
    _n = C.size();
    _m = C[0].size();

    if (_m == 1 && _n == 1) return 0;

    mapa.resize(_n);
    for (int i = 0; i < _n; i++)
    {
        mapa[i].resize(_m);
        for (int j = 0; j < _m; j++)
        {
            mapa[i][j] = INT_MAX;
        }
    }
    mapa[_n - 1][_m - 1] = 0;
    BackTrace(_m - 1, _n - 1, C, 0, 0);

    return mapa[0][0];
}


int main()
{
    std::vector<std::vector<int>> test{ {12, 6, 5, 3}, { 6, 13, 3, 15 }, { 8, 2, 6, 9 } };
	std::cout << getMinEffort(test);
}



